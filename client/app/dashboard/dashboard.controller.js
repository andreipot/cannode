'use strict';

angular.module('canApp')
  .controller('DashboardCtrl',['$scope','$q','$http','Searchword', 'Campaign','Auth', function ($scope,$q, $http, Searchword, Campaign,Auth) {
    $scope.message = 'Hello';

    //get all campaigns
    $scope.campaigns = {};
    $scope.completed = 0;


    var campaigns = Campaign.query(function(){
      console.log('all campaigns');
      console.log(campaigns);
      var user_id = Auth.getCurrentUser()._id;

      $scope.campaigns = _.filter(campaigns,function(data){
        return _.indexOf(data.user_id, user_id) > -1;
      });

      //get all keywords collection...
      $scope.searchwords = Searchword.query(function(){
        angular.forEach($scope.campaigns,function(campaign,key){
          campaign.serp_status = $scope.getCampaignSerpRate(campaign);
        });
        //got all campaign serp status
      });
    });
    //do serp
    //keyword is for searchword
    $scope.loadCampaigns = function() {
      var user_id = Auth.getCurrentUser()._id;
      $scope.campaigns = _.filter($scope.campaigns,function(data){
        return _.indexOf(data.user_id, user_id) > -1;
      });
      angular.forEach($scope.campaigns,function(campaign,key){
        campaign.serp_status = $scope.getCampaignSerpRate(campaign);
        console.log(campaign.serp_status);
      });
    }

    $scope.getCampaignSerpRate = function(campaign) {
      var deferred = $q.defer();
      var payload = {
        id: campaign._id
      };
      //call node API
      $http({
        method: 'post',
        url: '/api/campaigns/getserprate',
        params:payload
      })
        .success(function(data, status, headers, config) {
          deferred.resolve(data);
          campaign.serp_status = data;
        })
        .error(function(data){
          deferred.reject(data);
        })
      return deferred.promise;
    }
    $scope.loadCampaigns();

  }]);
