'use strict';

angular.module('canApp')
  .controller('SearchwordCtrl',['$scope','$q','$http','Searchword', 'Campaign', function ($scope,$q, $http, Searchword, Campaign) {
    $scope.message = 'Hello';

    //get all keywords collection...
    $scope.searchwords = Searchword.query();

    //get all campaigns
    $scope.campaigns = Campaign.query();
    //console.log($scope.campaigns);
    //serp/live
    $scope.rootURL = 'http://api.domaincrawler.com/v2/';
    $scope.searchword = {};
    $scope.campaign = {};

    $scope.completed = 0;
    //do serp
    //keyword is for searchword
    $scope.doSERP = function(searchword){

      var deferred = $q.defer();
      //updated searchword data
      var updatedata = new Searchword();

      var payload = {
        keyword          : searchword.keyword,
        searchengine_id  : searchword.searchengine_id,
        domain           : $scope.campaign.domain
      };

      //call node API
      $http({
        method: 'put',
        url: '/api/searchwords/serp',
        params:payload
      })
        .success(function(data, status, headers, config) {
            updatedata = Searchword.get({id : searchword._id},function(){
              updatedata.response = data;
              updatedata.$update(function(){
                console.log('done');
                deferred.resolve(data);
              });
            });
    })
      .error(function(data){
        deferred.reject(data);
      })
     return deferred.promise;
    };


    $scope.serpCampaign = function() {

      if(!$scope.campaign)
        return;
      //first get all campaign searchwords
      var deferred = $q.defer();
      //updated searchword data
      var updatedata = new Searchword();

      var payload = {
        id           : $scope.campaign._id
      };

      //call node API
      $http({
        method: 'post',
        url: '/api/campaigns/serp',
        params:payload
      })
        .success(function(data, status, headers, config) {
          console.log(data);
          deferred.resolve(data);
        })
        .error(function(data){
          deferred.reject(data);
        })
      return deferred.promise;
    }

    //get all search engines...
    var onResourceComplete = function(response) {
      $scope.allengines = JSON.parse(response.data);
      //  console.log('success');
      //  console.log(response.data);
    };

    var onError = function(reason) {
      $scope.error = "Could not fetch search engines";
      //console.log($scope.error);
    };

    $http.get('/api/campaigns/searchengines')
      .then(onResourceComplete, onError);

  }]);
