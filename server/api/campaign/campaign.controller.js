'use strict';

var _ = require('lodash');
var http = require('http');
var request = require('request');
var Campaign = require('./campaign.model');
var Searchword = require('../searchword/searchword.model');
// Get list of campaigns
exports.index = function(req, res) {

  Campaign.find(function (err, campaigns) {
    if(err) { return handleError(res, err); }
    return res.json(200, campaigns);
  });
};

// Get a single campaign
exports.show = function(req, res) {
  Campaign.findById(req.params.id, function (err, campaign) {
    if(err) { return handleError(res, err); }
    if(!campaign) { return res.send(404); }
    return res.json(campaign);
  });
};

// Get all keywords for a campaign
exports.serp = function(req,res){
  //console.log(req.query);
   Campaign.findById(req.query.id, function (err, campaign) {

    if(err) { return handleError(res, err); }
    if(!campaign) { return res.send(404); }

    //campaign retrived ,now searchwords
    //return res.json(campaign);

    //now retrives all searchwords
    Searchword.find({created_campaign_id: campaign.created_campaign_id},function(err,searchwords){

      var rootURL = 'http://api.domaincrawler.com/v2/serp/live?api_username=cem@copypanthers.com&api_key=4adca9f52d8719155f9c898a2b8c38da56364e48';
      //call node API
      searchwords.forEach(function(searchword){
       //for each search word do serp
        if(searchword.response==''){
          var payload = {
            keyword          : searchword.keyword,
            searchengine_id  : searchword.searchengine_id,
            domain           : campaign.domain
          };

          var params = '&keyword='+payload.keyword + '&searchengine_id='+payload.searchengine_id +'&domain='+payload.domain;
          var target_URL = rootURL +params;
          request(target_URL, function (error, res, body) {
            if (!error && res.statusCode == 200) {
              searchword.response = body;
              searchword.save(function (err) {
                if (err) { return handleError(res, err); }
                console.log('----saved-------');
              });
              //return res.json(searchword);
            }
          });
        }
        });
      //return res.json(searchwords);
      });
     return res.json(campaign);
    });
};

//get serp completion rate
exports.getSerpRate = function(req,res){
  Campaign.findById(req.query.id, function (err, campaign) {
    if(err) { return handleError(res, err); }
    if(!campaign) { return res.send(404); }
    //now retrives all searchwords
    Searchword.find({created_campaign_id: campaign.created_campaign_id},function(err,searchwords){
      var total = searchwords.length;
      var completed = 0;
      Searchword.find({created_campaign_id: campaign.created_campaign_id, response :{$ne:''}},function(err,searchwords) {
        completed = searchwords.length;
        return res.json({total: total, done: completed});
      });
    });
  });
}

// Creates a new campaign in the DB.
exports.create = function(req, res) {
  Campaign.create(req.body, function(err, campaign) {
    if(err) { return handleError(res, err); }
    return res.json(201, campaign);
  });
};

exports.searchengines = function(req, response) {
  var rootURL = 'http://api.domaincrawler.com/v2/searchengines?api_username=cem@copypanthers.com&api_key=4adca9f52d8719155f9c898a2b8c38da56364e48';
  request(rootURL, function (error, res, body) {
    if (!error && res.statusCode == 200) {
      response.json(body.toString('utf8'));
    }
  })
};
exports.createcampaign = function(req, response) {
  var rootURL = 'http://api.domaincrawler.com/v2/campaigns?api_username=cem@copypanthers.com&api_key=4adca9f52d8719155f9c898a2b8c38da56364e48';
  var payload = {
    name          : req.body.name,
    searchengine_id: req.body.searchengine_id
  };

  request.post({url:rootURL, form: payload}, function(err,res,body){
    //output data;
    if (err) {
      return console.error('upload failed:', err);
    }
    if (!err && res.statusCode == 200) {
      response.json(body.toString('utf8'));
    }
  })

};

// Updates an existing campaign in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Campaign.findById(req.params.id, function (err, campaign) {
    if (err) { return handleError(res, err); }
    if(!campaign) { return res.send(404); }
    var updated = _.merge(campaign, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, campaign);
    });
  });
};

// Deletes a campaign from the DB.
exports.destroy = function(req, res) {
  Campaign.findById(req.params.id, function (err, campaign) {
    if(err) { return handleError(res, err); }
    if(!campaign) { return res.send(404); }
    campaign.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}
